<?php

if (!defined('_PS_VERSION_')) exit;
if (defined('_PS_ADMIN_DIR_') === false) {define('_PS_ADMIN_DIR_', _PS_ROOT_DIR_.'/admin/');}
if (!isset($_SESSION)) {session_start();}

include_once(_PS_MODULE_DIR_ . 'resursbankpayment/resursdefaults.php');
include_once(_PS_MODULE_DIR_ . 'resursbankpayment/rbapiloader.php');


class ResursBankPayment extends PaymentModule
{
    public $configuredCredentials = false;
    public $rbapi = null;
    private $module_url = null;
    private $callback_url = null;
    private $regCallbackSuccess = false;
    const ENVIRONMENT_PRODUCTION = 0;
    const ENVIRONMENT_TEST = 1;

    public function __construct()
    {
        $this->name = 'resursbankpayment';
        $this->tab = 'payments_gateways';
        $this->version = '1.0.0';
        $this->author = 'Resurs Bank AB';
        $this->need_instance = 1;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = false;
		$this->controllers = array('resursajax');

        parent::__construct();

        //$this->context->link->getAdminLink('AdminResursBankPayment');
		//$modlink = $this->context->link->getModuleLink('resursbankpayment', 'resursbankpayment', array());
        //$link = new LinkCore;
        //$link->getAdminLink('resursbankpayment');

        $this->module_url = Tools::getProtocol(Tools::usingSecureMode()).$_SERVER['HTTP_HOST'].$this->getPathUri();
        $this->callback_url = $this->module_url . "resurscallback.php";

        $this->displayName = $this->l('Resurs Bank Payment Gateway');
        $this->description = $this->l('Accept payments through Resurs Bank');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
        $this->testMode = Configuration::get('RESURSBANK_MODE') == 'test';
    }

    public function setMedia()
    {
        $this->addJS(_THEME_JS_DIR_.'resursbankpayment.js');
        //$this->addCSS(_THEME_CSS_DIR_ . 'resursbank.css');
    }
    public function HandleErrors($errors = array(), $step = null) {
        if (is_array($errors)) {
            $errorMessage = implode(". ", $errors);
        } else {
            $errorMessage = $errors;
        }
        if ($errorMessage) {
            //$this->context->cookie->__set('redirect_errors', Tools::displayError($errorMessage));
            $url = Tools::getProtocol(Tools::usingSecureMode()) . Tools::getHttpHost(false, true) . __PS_BASE_URI__;
            Tools::redirect($url . 'index.php?controller=order'.(!is_null($step) && $step > 0 ? "&step=" . $step : "").'&resursError=' . rawurlencode($errorMessage));
        }
    }

    private function getResursVars()
    {
        $RBLoader = new ResursBankLoader();
        return $RBLoader->getResursVars();
    }

    private function createOrderState($name = array(), $settings = array()) {
        $db = Db::getInstance();
        $db->insert("order_state", array(
            "id_order_state" => "null",
            "invoice" => isset($settings["invoice"]) ? $settings["invoice"] : 1,
            "send_email" => isset($settings["send_email"]) ? $settings["send_email"] : 0,
            "module_name" => "resursbankpayment",
            "color" => isset($settings["color"]) ? $settings["color"] : 'Orange',
            "unremovable" => isset($settings["unremovable"]) ? $settings["unremovable"] : 1,
            "hidden" => isset($settings["hidden"]) ? $settings["hidden"] : 0,
            "logable" => isset($settings["logable"]) ? $settings["logable"] : 1,
            "delivery" => isset($settings["deliver"]) ? $settings["delivery"] : 0,
            "shipped" => isset($settings["shipped"]) ? $settings["shipped"] : 0,
            "paid" => isset($settings["paid"]) ? $settings["paid"] : 0,
            "deleted" => isset($settings["deleted"]) ? $settings["deleted"] : 0
        ));
        $stateId = $db->Insert_ID();

        echo "StateID for $name = $stateId<br>\n";

        $languages = $db->executeS("SELECT id_lang, iso_code FROM " . _DB_PREFIX_ . "lang WHERE iso_code IN('sv','en')");
        foreach ($languages as $language) {
            if (isset($name[$language['iso_code']])) {
                $db->insert('order_state_lang', array(
                    "id_order_state" => pSQL($stateId),
                    "id_lang" => pSQL($language['id_lang']),
                    "name" => $name[$language['iso_code']],
                    "template" => "payment"
                ));
            }
        }
        return $stateId;
    }

	public function install()
	{
        $resursInstallSuccess = true;
        $orderStates = Db::getInstance()->executeS("SELECT id_order_state,paid FROM " . _DB_PREFIX_ . "order_state WHERE module_name = 'resursbankpayment'");
        $vars = $this->getResursVars();
        if (!count($orderStates)) {
            $orderStatePending = $this->createOrderState(array('sv' => 'Resurs Bank v�ntande', 'en' => 'Resurs Bank pending'));
            $orderStatePaid = $this->createOrderState(array('sv' => 'Betald med Resurs Bank', 'en' => 'Paid with Resurs Bank'), array('paid'=>1, 'send_email'=>1, 'color' => '#32CD32'));
            $vars['defaults']['RESURS_STATE_PENDING'] = $orderStatePending;
            $vars['defaults']['RESURS_STATE_PAID'] = $orderStatePaid;
            $vars['conf']['RESURS_STATE_PENDING'] = $orderStatePending;
            $vars['conf']['RESURS_STATE_PAID'] = $orderStatePaid;
        } else {

            foreach ($orderStates as $orderState) {
                if ($orderState['paid']) {
                    $vars['defaults']['RESURS_STATE_PAID'] = $orderState['id_order_state'];
                    $vars['conf']['RESURS_STATE_PAID'] = $orderState['id_order_state'];
                } else {
                    $vars['defaults']['RESURS_STATE_PENDING'] = $orderState['id_order_state'];
                    $vars['conf']['RESURS_STATE_PENDING'] = $orderState['id_order_state'];
                }
            }
        }
        foreach ($vars['defaults'] as $resursVar => $resursDefault) {
            if (!Configuration::updateValue($resursVar, $resursDefault)) {
                $resursInstallSuccess = false;
            }
        }
        if (!parent::install() OR !$resursInstallSuccess OR !$this->registerHook('payment') OR !$this->registerHook('paymentReturn') OR !$this->registerHook('header')) return false;
        return true;
	}
    public function uninstall() {
        $vars = $this->getResursVars();
        foreach ($vars['defaults'] as $resursVar => $resursDefault) {Configuration::deleteByName($resursVar);}
        return (parent::uninstall());
    }

    // TODO: move this somewhere, for all pre-initializations
    private function initAPI()
    {
        $resursVars = $this->getResursVars();
        $conf = $resursVars['conf'];
        $p_environment = $conf['RESURS_ENVIRONMENT'];

        $this->rbapi = new ResursBank($conf['RESURS_REPRESENTATIVE_ID'], $conf['RESURS_REPRESENTATIVE_PASSWORD']);
        $this->rbapi->environment = ($p_environment == 'test' ? self::ENVIRONMENT_TEST : self::ENVIRONMENT_PRODUCTION);
    }

    public function mkpass()
    {
        $retp = null;
        $characterListArray = array(
            'abcdefghijklmnopqrstuvwxyz',
            'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
            '0123456789',
            '!@#$%*?'
        );
        $chars = array();
        $max = 10; // This is for now static
        foreach ($characterListArray as $charListIndex => $charList) {for ($i = 0 ; $i <= ceil($max/sizeof($characterListArray)) ; $i++) {$chars[] = $charList{mt_rand(0, (strlen($charList) - 1))};}}
        shuffle($chars);
        $retp = implode("", $chars);
        return $retp;
    }

    public function registerCallbacks($conf = array())
    {
        $callbackSaveData = array();
        if (empty($conf)) {
            $resursVars = $this->getResursVars();
            $conf = $resursVars['conf'];
        }
        if (empty($this->rbapi)) { $this->initAPI(); }

        $parameter = array(
            'ANNULMENT' => array('paymentId'),
            'AUTOMATIC_FRAUD_CONTROL' => array('paymentId', 'result'),
            'FINALIZATION' => array('paymentId'),
            'UNFREEZE' => array('paymentId')
        );

        foreach ($parameter as $callbackType => $parameterArray) {
            $digestSaltString = $this->mkpass();
            $digestArray = array(
                'digestSalt' => $digestSaltString,
                'digestParameters' => $parameterArray
            );

            if ($callbackType == "ANNULMENT") {$setCallbackType = ResursCallbackTypes::ANNULMENT;}
            if ($callbackType == "AUTOMATIC_FRAUD_CONTROL") {$setCallbackType = ResursCallbackTypes::AUTOMATIC_FRAUD_CONTROL;}
            if ($callbackType == "FINALIZATION") {$setCallbackType = ResursCallbackTypes::FINALIZATION;}
            if ($callbackType == "UNFREEZE") {$setCallbackType = ResursCallbackTypes::UNFREEZE;}

            $renderArray = array();
            if (is_array($parameterArray)) {
                foreach ($parameterArray as $parameterName) {
                    $renderArray[] = $parameterName . "={".$parameterName."}";
                }
            }
            $callbackURL = $conf['RESURS_CALLBACK_URL'] . "?event=".$callbackType."&digest={digest}&" . implode("&", $renderArray);
            try {
                $callbackSetResult = $this->rbapi->setCallback($setCallbackType, $callbackURL, $digestArray);
                if (!empty($this->rbapi->lastError)) {
                    // Do something here
                    return $this->rbapi->lastError;
                }
                if ($callbackSetResult) {
                    $callbackSaveData[$callbackType] = array('salt' => $digestSaltString);
                }
            }
            catch (Exception $regCallbackException)
            {

            }
        }
        if (count($callbackSaveData) == count($parameter)) {
            Configuration::updateValue("RESURS_CALLBACK_DIGEST", strval(serialize($callbackSaveData)));
            return true;
        } else {
            return false;
        }
    }

    private function linkJS() {
        $this->context->controller->addJS($this->_path . 'js/resursajax.js');
        $this->context->controller->addJS($this->_path . 'js/resurspayment_admin.js');
        $this->context->controller->addCSS($this->_path . 'css/resursbank.css');
        //$_controller = Context::getContext()->controller;
        //$_controller->addCSS($this->path . 'css/resursbank.css');
        //$_controller->addJs($this->_path . 'js/resurspayment_admin.js');
        //$_controller->addJs($this->_path . 'js/resursajax.js');
    }

    public function hookHeader() {
        $resursError = Tools::getValue("resursError");
        if (!empty($resursError)) {
            echo $this->displayError(htmlentities($resursError));
        }
    }

    // is_configurable
    public function getContent()
    {
        $resursVars = $this->getResursVars();
        $conf = $resursVars['conf'];

        $this->linkJS();

        if (isset($_POST['getPaymentMethods'])) {
            $this->initAPI();
            //$meth = $this->rbapi->paymentMethodList();
            $this->rbapi->getPaymentMethods();
            $paymentMethods = $this->rbapi->getPaymentMethodsArray();
            if (count($paymentMethods)) {
                Configuration::updateValue('RESURS_PAYMENT_METHODS', strval(serialize($paymentMethods)));
            }
        }

        $this->_html = '<h2>' . $this->l('Resurs Bank Configuration') . '</h2>';
        if (isset($_POST['resursSaveConfig'])) {
            // This part should also open for more configuration values
            $resursVars = $this->getResursVars();
            foreach ($resursVars['conf'] as $resursVar => $resursValue) {
                if ($resursVar == 'RESURS_STATE_PENDING' || $resursVar == 'RESURS_STATE_PAID')
                {
                    // Values that's not in forms
                    Configuration::updateValue($resursVar, strval($resursVars['conf'][$resursVar]));
                } else {
                    if (isset($_POST[$resursVar])) {
                        Configuration::updateValue($resursVar, strval($_POST[$resursVar]));
                    }
                }
            }
            /*
             * Prepare for callbacks on each save, so there will always be a fresh digest available.
             *
             * Four callbacks will be prepared.
             * ANNULMENT, AUTOMATIC_FRAUD_CONTROL, UNFREEZE, FINALIZATION
             *
             */
            if (isset($_POST['RESURS_CALLBACK_URL']) && !empty($_POST['RESURS_CALLBACK_URL'])) {
                $conf['RESURS_CALLBACK_URL'] = $_POST['RESURS_CALLBACK_URL'];
                $this->regCallbackSuccess = $this->registerCallbacks($conf);
            }
        }
        $this->configurationForm();
        return $this->_html;
    }

    public function hookPayment($params) {
        global $smarty;
        $this->linkJS();
        if (!$this->active)
            return;
        if (!$this->_checkCurrency($params['cart']))
            return;

        // For next step
        /*
        $cart = $params['cart'];
        $products = $cart->getProducts(true);

        $discounts = $cart->getDiscounts();
        */

        /*
        echo "<pre>";
        print_r($summary);
        print_r($discounts);
        print_r($cart);
        echo "</pre>";
        foreach ($products as $row => $prodArray) {
        }
        */
        $summary = $params['cart']->getSummaryDetails();
        $limitOk = false;
        $totalPrice = $summary['total_price'];

        $pMethodsConfig = unserialize(Configuration::get('RESURS_PAYMENT_METHODS_CONFIGURATION'));
        $pMethodsInfoArray = unserialize(Configuration::get('RESURS_PAYMENT_METHODS'));
        $pMethodsInfo = array();
        foreach ($pMethodsInfoArray as $pMethodCount => $pMethodArray) {
            $id = $pMethodArray['id'];
            $pMethodsInfo[$id] = $pMethodArray;
        }
        $methodBits = "";
        $errorArray = array();
        $errorQuery = Tools::getValue("errors");

        /* {include file="$tpl_dir./errors.tpl"} */
        //$tpl_dir = _PS_THEME_DIR_;

        if (!empty($errorQuery)) {
            $errorArray[] = $errorQuery;
        }
        foreach ($pMethodsConfig as $method => $methodArray)
        {
            $minLimit = $pMethodsInfo[$method]['minLimit'];
            $maxLimit = $pMethodsInfo[$method]['maxLimit'];
            if ($totalPrice <= $minLimit || $totalPrice >= $maxLimit) { $methodArray['active'] = false;}
            if ($methodArray['active'])
            {
                $methodValues = array();
                $methodValues["resurspayment_id"] = $method;
                foreach ($pMethodsInfo[$method] as $pMethodParam => $pMethodValue) {
                    if (!is_array($pMethodValue) && !empty($pMethodValue)) {
                        $methodValues["resurspayment_" . $pMethodParam] = $pMethodValue;
                    }
                }
                foreach ($methodArray as $pMethodParam => $pMethodValue) { $methodValues["resurspayment_" . $pMethodParam] = $pMethodValue; }
                $this->context->smarty->assign($methodValues);
                $methodBits .= $this->display(__FILE__, 'resursbankmethodbits.tpl');
            }
        }
        $this->context->smarty->assign(array('availablePaymentMethods' => $methodBits, 'errors' => $errorArray));

        return $this->display(__FILE__, 'resursbankpayment.tpl');
    }

    public function checkCurrency($cart)
    {
        $currency_order = new Currency($cart->id_currency);
        $currencies_module = $this->getCurrency($cart->id_currency);
        if (is_array($currencies_module)) {foreach ($currencies_module as $currency_module) { if ($currency_order->id == $currency_module['id_currency']) {return true;} }}
        return false;
    }

    private function _checkCurrency($cart) {
        $currency_order = new Currency(intval($cart->id_currency));
        $currencies_module = $this->getCurrency();
        $currency_default = Configuration::get('PS_CURRENCY_DEFAULT');

        // TODO: CHANGE THIS TO SUPPORT NORDIC CURRENCIES INSTEAD (CHECK BY COUNTRY)
        $allowedCurrencies = array('SEK', 'NOK', 'DKK', 'EUR');
        //if (strtoupper($currency_order->iso_code) != 'SEK' && strtoupper($currency_order->iso_code) != 'EUR') {return;}


        if (is_array($currencies_module))
            foreach ($currencies_module AS $currency_module)
                if ($currency_order->id == $currency_module['id_currency'])
                    return true;
    }
    public function translate($key) {
        array(
            $this->l('You can not use a company with this payment method')
        );
        return $this->l($key);
    }

    public function configurationForm()
    {
        $resursVars = $this->getResursVars();
        $conf = $resursVars['conf'];
        $current = $conf;
        $methodDisplay = "";

        foreach ($conf as $confParameter => $confValue) {
            if (array_key_exists($confParameter, $_POST)) {$current[$confParameter] = $_POST[$confParameter];}
        }

        $saltTest = @unserialize($conf['RESURS_CALLBACK_DIGEST']);
        $callbackEvents = array();
        if (count($saltTest)) {
            foreach ($saltTest as $eventType => $eventArray) {
                $callbackEvents[] = $eventType;
            }
        }

        $pMethods = unserialize($current['RESURS_PAYMENT_METHODS']);
        $pMethodsConfig = unserialize($current['RESURS_PAYMENT_METHODS_CONFIGURATION']);
        if (empty($current['RESURS_CALLBACK_URL'])) { $current['RESURS_CALLBACK_URL'] = $this->callback_url; }

        $this->_html .= '
        <form action="' . $_SERVER['REQUEST_URI'] . '" method="post" style="clear: both;">

        <fieldset>
        <legend><img src="../img/admin/contact.gif" />' . $this->l('Settings') . '</legend>
				<div class="warn">
					' . $this->l('Module version: ') . $this->version . '
				</div>
				<br />
				<br />
				<h3>' . $this->l('Resurs Bank Webservices and API') . '</h3>
				' . $this->l('Current environment:') . '
				<select name="RESURS_ENVIRONMENT">
						<option value="production"' . (Configuration::get('RESURS_ENVIRONMENT') == 'production' ? ' selected="selected"' : '') . '>' . $this->l('Production') . '</option>
						<option value="test"' . (Configuration::get('RESURS_ENVIRONMENT') == 'test' ? ' selected="selected"' : '') . '>' . $this->l('Test') . '</option>
				</select><br />
				<span>' . $this->l('Production:') . '</span> <input type="text" size="50" name="RESURS_ENVIRONMENT_PRODUCTION" value="' . htmlentities($current['RESURS_ENVIRONMENT_PRODUCTION'], ENT_COMPAT, 'UTF-8') . '" /><br />
				<span>' . $this->l('Test:') . '</span> <input type="text" size="50" name="RESURS_ENVIRONMENT_TEST" value="' . htmlentities($current['RESURS_ENVIRONMENT_TEST'], ENT_COMPAT, 'UTF-8') . '" /><br />
				<h3>' . $this->l('Resurs Bank login credentials') . '</h3>
				' . $this->l('Customer ID:') . ' <input type="text" size="50" name="RESURS_REPRESENTATIVE_ID" value="' . htmlentities($current['RESURS_REPRESENTATIVE_ID'], ENT_COMPAT, 'UTF-8') . '" /><br />
				' . $this->l('Customer Password:') . ' <input type="password" size="50" name="RESURS_REPRESENTATIVE_PASSWORD" value="' . htmlentities($current['RESURS_REPRESENTATIVE_PASSWORD'], ENT_COMPAT, 'UTF-8') . '" /><br />

				<h3>' . $this->l('Resurs Bank API') . '</h3>
				' . $this->l('Callback URL:') . ' <input type="text" size="80" name="RESURS_CALLBACK_URL" value="' . htmlentities($current['RESURS_CALLBACK_URL'], ENT_COMPAT, 'UTF-8') . '" /><br />
                ' . $this->l('Callback events registered:') . ' <i>' .(count($callbackEvents) && $this->regCallbackSuccess === true ? implode(", ", $callbackEvents) : (!is_string($this->regCallbackSuccess) ? $this->l('None. Update settings to update callback events') : $this->l('Error received: ') . '<b>' . $this->regCallbackSuccess . '</b>')) . '</i><br />

        <input type="submit" name="resursSaveConfig" value="' . $this->l('Update settings') . '" class="button" />
        </fieldset>
        </form>
        ';

        if ($current['RESURS_REPRESENTATIVE_ID'] != "")
        {
            if (is_array($pMethods) && count($pMethods) > 0) {
                foreach ($pMethods as $methodIdx => $methodArray) {
                    if (gettype($methodArray) === "object") {continue;}
                    // id, description, legalInfoLinks [appendPriceLast, endUserDescription, url], minLimit, maxLimit, type, customerType, specificType
                    $methodDisplay .= '<fieldset>
                        <table width="800px" style="border:none;" id="resursPaymentMethodsForm[' . $methodArray['id'] . ']">
                        <tr><td colspan="2" style="font-weight: bold;padding: 5px;">' . $methodArray['id'] . ": " . $methodArray['description'] . '</td></tr>
                        <tr><td style="padding: 5px;">' . $this->l('Active') . '</td><td><select id="resurs_method[' . $methodArray['id'] . '][active]"><option value="0">' . $this->l('No') . '</option><option value="1" ' . (isset($pMethodsConfig[$methodArray['id']]['active']) && $pMethodsConfig[$methodArray['id']]['active'] == "1" ? "selected=selected" : "") . '>' . $this->l('Yes') . '</option></select></td></tr>
                        <tr><td style="padding: 5px;">' . $this->l('Title') . '</td><td><input type="text" size="50" id="resurs_method[' . $methodArray['id'] . '][title]" value="' . (isset($pMethodsConfig[$methodArray['id']]['title']) ? htmlentities(utf8_decode($pMethodsConfig[$methodArray['id']]['title'])) : "") . '"></td></tr>
                        <tr><td style="padding: 5px;">' . $this->l('Payment method description') . '</td><td><input type="text" size="50" id="resurs_method[' . $methodArray['id'] . '][description]" value="' . (isset($pMethodsConfig[$methodArray['id']]['description']) ? htmlentities(utf8_decode($pMethodsConfig[$methodArray['id']]['description'])) : "") . '"></td></tr>
                        <tr><td style="padding: 5px;">' . $this->l('Sort order') . '</td><td><input type="text" size="50" id="resurs_method[' . $methodArray['id'] . '][sort_order]" value="' . (isset($pMethodsConfig[$methodArray['id']]['sort_order']) ? htmlentities(utf8_decode($pMethodsConfig[$methodArray['id']]['sort_order'])) : ""). '"></td></tr>
                        <!-- TODO: Logotype prytzl-->
                        <tr><td style="padding: 5px;">' . $this->l('Logotype') . '</td><td><input type="text" size="50" id="resurs_method[' . $methodArray['id'] . '][logotype]" value="' . (isset($pMethodsConfig[$methodArray['id']]['logotype']) ? htmlentities(utf8_decode($pMethodsConfig[$methodArray['id']]['logotype'])) : ""). '"></td></tr>
                        <tr><td style="padding: 5px;">' . $this->l('Method payment fee') . '</td><td><input type="text" size="50" id="resurs_method[' . $methodArray['id'] . '][payment_fee]"value="' . (isset($pMethodsConfig[$methodArray['id']]['payment_fee']) ? htmlentities(utf8_decode($pMethodsConfig[$methodArray['id']]['payment_fee'])) : "") . '"></td></tr>
                        <tr><td style="padding: 5px;">' . $this->l('Payment fee description') . '</td><td><input type="text" size="50" id="resurs_method[' . $methodArray['id'] . '][payment_fee_description]"value="' . (isset($pMethodsConfig[$methodArray['id']]['payment_fee_description']) ? htmlentities(utf8_decode($pMethodsConfig[$methodArray['id']]['payment_fee_description'])) : "") . '"></td></tr>
                        <tr><td style="padding: 5px;">' . $this->l('Customer type') . '</td><td>' . (isset($methodArray['customerType']) ? $methodArray['customerType'] : "?") . '</td></tr>
                        </table>
                        </fieldset>';
                }

                $methodDisplay .= '
                    <input type="button" class="button" onclick="ajax_rb_save_methods()" value="'.$this->l('Save payment methods configuration').'"><span style="margin:5px;padding:5px;" id="savePaymentMethodStatus"></span>
                ';
            } else {
                $methodDisplay .= '
                <b><i>'.$this->l('No payment methods has been configured yet!').'</i></b><br />
                <br />
                ';
            }

            $this->_html .= '
            <h3>'.$this->l('Resurs Payment Methods').'</h3>
            <!--<fieldset>-->
            <div id="resursPaymentMethods">
            '.$methodDisplay . '
            </div>
            <!--</fieldset>-->
             <form action="' . $_SERVER['REQUEST_URI'] . '&resapi=getPaymentMethods" method="post" style="clear: both;">
            <input type="submit" name="getPaymentMethods" value="'.$this->l('Get payment methods').'" class="button"><br>
            </form>
            ';
        }


        return $this->_html;
    }
}

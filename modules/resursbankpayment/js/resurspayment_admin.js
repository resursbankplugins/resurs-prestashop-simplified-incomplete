function ajax_rb_save_methods()
{
    var postData = {}
    var paymentMethodData = {}
    $("#savePaymentMethodStatus").html("");

    $("#resursPaymentMethods").find(":input").each(
        function() {
            var paymentid = "";
            var paymentvar = "";
            var paymentval = "";
            paymentid = this.id.substr(this.id.indexOf("[")+1, this.id.indexOf("]")-this.id.indexOf("[")-1);
            paymentvar = this.id.substr(this.id.indexOf("][")+2);
            paymentvar = paymentvar.substr(0, paymentvar.length -1);
            //postVar += "&" + this.id + "=" + escape(this.value);
            if (paymentid != "") {
                if (typeof paymentMethodData[paymentid] === "undefined") {
                    paymentMethodData[paymentid] = {};
                }
                paymentMethodData[paymentid][paymentvar] = this.value;
            }
        }
    );
    if (typeof paymentMethodData === "object") {
        postData = {"paymentMethods": paymentMethodData}
        resfetch("savePaymentMethodConfig", postData, "savePaymentMethodConfig");
    }
}

function savePaymentMethodConfig(ajaxResponse) {
    if (ajaxResponse.errors.success)
    {
        $("#savePaymentMethodStatus").html('<img src="/modules/resursbankpayment/yes.png">');
    }
    else
    {
        $("#savePaymentMethodStatus").html('<img src="/modules/resursbankpayment/no.png">');
    }
}

function paymentform(ajaxResponse) {
    if (resIsSuccess(ajaxResponse)) {
        if (typeof getResResponse(ajaxResponse).template !== "undefined") {
            var paymentMethod = getResResponse(ajaxResponse).paymentMethod;
            var formData = getResResponse(ajaxResponse).template;
            $('.resursbank_pay_module').prepend(formData);
        }
    }
}


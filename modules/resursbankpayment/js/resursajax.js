function resfetch(resMethod) {
    var baseUrl = "";
    var method = "GET";
    var callback = "";
    var postData = {req: resMethod, ajax: true};
    // Put resfetch into a path we know, before usage
    if (typeof baseDir !== "undefined") {
        baseUrl += baseDir + "/modules/resursbankpayment/ajax.php?do=" + resMethod;
    } else {
        baseUrl += "/modules/resursbankpayment/ajax.php?do=" + resMethod;
    }
    if (typeof arguments[1] === "string") {
        callback = arguments[1];
    }
    if (typeof arguments[2] === "string") {
        if (typeof arguments[1] === "object") {
            method = "POST";
            postData = arguments[1];
        }
        callback = arguments[2];
    }
    else {
        if (typeof arguments[1] === "object") {
            method = "POST";
            postData = arguments[1];
            callback = resMethod;
        }
    }
    $.ajax({
            type: method,
            url: baseUrl,
            data: postData
        }
    ).done(
        function resursReturn(returnedData) {
            // Handle calls automatically by checking for existing methods before usage
            if (callback != "") {
                if (typeof window[callback] === "function") {
                    window[callback](returnedData);
                }
            }
        }
    ).error(
        //if (typeof console != "undefined") {}
    );
}

function fetchResursError(message) {
    var messageDestination = null;
    if (typeof arguments[1] !== "undefined") {
        messageDestination = arguments[1];
    }
    resfetch("error", { "message":message, "messagedestination": messageDestination }, 'resursAjaxError');
    // http://mock.prestashop.cte.loc/modules/resursbankpayment/ajax.php?do=error&message=Fail
}

function resursAjaxError(data) {
    if (typeof getResResponse(data).template !== "undefined") {
        if (typeof getResResponse(data).messagedestination !== "undefined") {
            $("#" + getResResponse(data).messagedestination).html(getResResponse(data).template);
        }
    }
}

function resIsSuccess(resursResponse) {
    if (typeof resursResponse !== "undefined" && typeof resursResponse.errors !== "undefined" && typeof resursResponse.errors.success !== "undefined") {
        if (resursResponse.errors.success === true) {
            return true;
        }
    }
    return false;
}
function getResResponse(resursResponse) {
    if (typeof resursResponse !== "undefined" && typeof resursResponse.response !== "undefined") {return resursResponse.response;}
}

function paymentFormStart() {
    //fetchResursError("You have done something very bad!", "resursPaymentErrors");
    // Set true to false when ready to do dynamic finish
    return true;
    return false;
}


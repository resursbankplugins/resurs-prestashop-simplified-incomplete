<?php

/*
 * AJAX Helper.
 *
 */

include_once(dirname(__FILE__) . '/../../config/config.inc.php');
include_once(dirname(__FILE__) . '/../../init.php');
include_once(dirname(__FILE__) . '/rbapiloader.php');
include_once(_PS_MODULE_DIR_ . 'resursbankpayment/resursdefaults.php');
include_once(_PS_MODULE_DIR_ . 'resursbankpayment/resursbankpayment.php');

$modDir = _PS_MODULE_DIR_ . 'resursbankpayment';

function resursAjaxError($faultString = '')
{
    $ajaxError = array(
        'errors' => array(
            'success' => false,
            'faulstring' => $faultstring
        )
    );
    header("Content-type: application/json");
    echo json_encode($ajaxError);
    exit;
}
function resursJsonResponse($responseArray = array())
{
    $ajaxResponse = array(
        'response' => $responseArray,
        'errors' => array(
            'success' => true,
            'faultstring' => ''
        )
    );
    header("Content-type: application/json");
    echo json_encode($ajaxResponse);
    exit;
}
function resursAjaxSuccess()
{
    header("Content-type: application/json");
    echo json_encode(array('errors' => array("success"=>true, "faultstring"=>'')));
    exit;
}

// defined('_PS_ADMIN_DIR_')
if (!isset($_SESSION)) {session_start();}
//$cookie = new Cookie('ps');
$adminCookie = new Cookie('psAdmin');

$RBLoader = new ResursBankLoader();
$RBPlugin = new ResursBankPayment();
$resursVars = $RBLoader->getResursVars();
$config = $resursVars['conf'];

$do = Tools::getValue("do");
$errorTemplate = _PS_THEME_DIR_ . "./errors.tpl";

if ($do == "error") {
    if (file_exists($errorTemplate)) {
        $smarty->assign(array('errors' => Tools::getValue("message")));
        $messageDestination = Tools::getValue('messagedestination');
        resursJsonResponse(array('template' => $smarty->fetch($errorTemplate), 'messagedestination' => $messageDestination), true, null);
    }
    exit;
}

if ($do == "paymentform") {
    $formType = Tools::getValue("type");
    //$formTemplate = _PS_MODULE_DIR_ . 'resursbankpayment/forms/' . strtolower($formType) . ".tpl";
    $formTemplate = _PS_MODULE_DIR_ . 'resursbankpayment/forms/paymentformtemplate.tpl';    // Using the same for all, setting up what's needed only
    $paymentMethod = Tools::getValue("paymentMethod");
    $templateHtml = "";
    $showFields = array();

    $summary = $cart->getSummaryDetails();
    $limitOk = false;
    $totalPrice = $summary['total_price'];

    $address = new Address(intval($cart->id_address_invoice));
    $methods = @unserialize($config['RESURS_PAYMENT_METHODS']);
    // Must find valid method
    $chosenMethod = array();
    $minLimit = 0;
    $maxLimit = 0;
    if (is_array($methods)) {
        foreach ($methods as $methodIdx => $methodIdArray) {
            if (isset($methodIdArray['id']) && strtolower($methodIdArray['id']) == strtolower($paymentMethod)) {
                $chosenMethod = $methodIdArray;
                $minLimit = $methodIdArray['minLimit'];
                $maxLimit = $methodIdArray['maxLimit'];
            }
        }
    }
    $customerType = (isset($chosenMethod['customerType']) ? $chosenMethod['customerType'] : "NATURAL");

    $errors = array();
    $showFields = array(
        'applicant-government-id' => false,
        'applicant-telephone-number' => false,
        'applicant-mobile-number' => false,
        'applicant-email-address' => false,
        'card-number' => false,
        'applicant-full-name' => false,
        'contact-government-id' => false,
        'finish-button' => true
    );

    $addressValues = array(
        'applicant-full-name' => $address->firstname . " " . $address->lastname,
        'applicant-telephone-number' => (!empty($address->phone) ? $address->phone : !empty($address->phone_mobile) ? $address->phone_mobile : ""),
        'applicant-mobile-number' => (!empty($address->phone_mobile) ? $address->phone_mobile : !empty($address->phone) ? $address->phone : "")
    );

    $blockPayment = false;
    if ($totalPrice >= $minLimit && $totalPrice <= $maxLimit)
    {
        $limitOk = true;
    }
    else
    {
        $blockPayment = true;
        $errors[] = $RBPlugin->l('Sorry, this payment method is currently not available');
    }
    if (file_exists($formTemplate)) {

        if ($customerType == "NATURAL") {
            if (!empty($address->company)) {
                $errors[] = $RBPlugin->l('You can not use a company with this payment method');
                $blockPayment = true;
            } else {
                $showFields['applicant-government-id'] = true;
            }
        } elseif ($customerType == "LEGAL") {
            if ($formType != "INVOICE") {
                $errors = array();
                $errors[] = $RBPlugin->l('Companies are not supported with this payment method');
                $blockPayment = true;
            }
            if (empty($address->company)) {
                $errors[] = $RBPlugin->l('You must enter a company name with this payment method');
                $blockPayment = true;
            } else {
                $showFields['applicant-government-id'] = true;
                $showFields['applicant-full-name'] = true;
                $showFields['contact-government-id'] = true;
            }
        } else {
            $showFields['applicant-government-id'] = true;
        }

        if (!$blockPayment) {
            if ($formType == "CARD") {
                $showFields['card-number'] = true;
            } elseif ($formType == "INVOICE") {
                $showFields['applicant-telephone-number'] = true;
                $showFields['applicant-mobile-number'] = true;
                $showFields['applicant-email-address'] = true;
                if ($customerType == "NATURAL") {
                    $showFields['applicant-government-id'] = true;
                    $showFields['applicant-full-name'] = false;
                    $showFields['contact-government-id'] = false;
                } else {
                    $showFields['applicant-full-name'] = true;
                    $showFields['contact-government-id'] = true;
                }
            } elseif ($formType == "REVOLVING_CREDIT") {
                // Nothing else than a government id is needed here, and that's a default setting
            }
        } else {
            foreach ($showFields as $fieldType => $fieldFormerValueIgnored) { $showFields[$fieldType] = false; }
        }

        $smarty->assign(array(
            'resurspayment_id' => $paymentMethod,
            'formType' => $formType,
            'showFields' => $showFields,
            'customerType' => $customerType,
            'paymentMethodData' => $chosenMethod,
            'addressValues' => $addressValues,
            'errors' => $errors
        ));

        $templateHtml = $smarty->fetch($formTemplate);
        $setErrorTemplate = $smarty->fetch($errorTemplate);
        resursJsonResponse(array('template' => $setErrorTemplate . $templateHtml, 'paymentMethod' => $paymentMethod), true, null);
    }
}


/*
 * Calls only available if admin is logged in during session, which is required to update payment methods in a session that takes place in the admin control panel
 *
 */
if ($adminCookie->isLoggedBack()) {
    if ($config['RESURS_REPRESENTATIVE_ID'] != "") {
        try {
            $rbapi = new ResursBank($config['RESURS_REPRESENTATIVE_ID'], $config['RESURS_REPRESENTATIVE_PASSWORD']);
        } catch (Exception $e) {
            resursAjaxError($e->getMessage());
            exit;
        }
        if ($do == "savePaymentMethodConfig") {
            $paymentMethods = Tools::getValue("paymentMethods");
            if (is_array($paymentMethods)) {
                if (Configuration::updateValue('RESURS_PAYMENT_METHODS_CONFIGURATION', strval(serialize($paymentMethods)))) {
                    resursAjaxSuccess();
                }
            }
        }
    }
}

/*
 * Callbacks from Ecommerce.
 * Requirements for a proper call: Salt/Digest
 *
 */


exit;
<span id="resursPaymentErrors"></span>

<!-- {$formType} {$customerType} -->

<form id="orderForm_{$resurspayment_id}" action="{$base_dir_ssl}modules/resursbankpayment/finishorder.php" method="post" onsubmit="return paymentFormStart()">

    <input type="hidden" name="resurs_payment_method" value="{$resurspayment_id}">
    <input type="hidden" name="resurs_type" value="{$customerType}">
    <table width="100%" cellpadding="10" cellspacing="0" style="border:1px solid #CCCCCC;">

    {if $showFields['card-number']}
    <tr>
        <td>{l s='Card number' mod='resursbankpayment'}</td>
        <td><input id="card-number" name="card-number" type="text" maxlength="19"></td>
    </tr>
    {/if}
    {if $showFields['applicant-government-id']}
        <tr>
            <td>
                {if $customerType == "NATURAL"}
                    {l s='Social security number' mod='resursbankpayment'}
                {else}
                    {if $formType == "INVOICE"}
                        {l s='Corporate government identity' mod='resursbankpayment'}
                    {else}
                        {l s='Social security number' mod='resursbankpayment'}
                    {/if}
                {/if}
            </td>
            <td><input id="applicant-government-id" name="applicant-government-id" type="text" maxlength="32"></td>
        </tr>
    {/if}

    {if $showFields['applicant-full-name']}
        <tr>
            <td>{l s='Applicant full name' mod='resursbankpayment'}</td>
            <td><input id="applicant-full-name" name="applicant-full-name" type="text" maxlength="100" value="{$addressValues['applicant-full-name']}"></td>
        </tr>
    {/if}
    {if $showFields['contact-government-id']}
        <tr>
            <td>{l s='Contact government id' mod='resursbankpayment'}</td>
            <td><input id="contact-government-id" name="contact-government-id" type="text" maxlength="100"></td>
        </tr>
    {/if}

    {if $showFields['applicant-telephone-number']}
        <tr>
            <td>{l s='Telephone number' mod='resursbankpayment'}</td>
            <td><input id="applicant-telephone-number" name="applicant-telephone-number" type="text" maxlength="32" value="{$addressValues['applicant-telephone-number']}"></td>
        </tr>
    {/if}

    {if $showFields['applicant-mobile-number']}
        <tr>
            <td>{l s='Mobile number' mod='resursbankpayment'}</td>
            <td><input id="applicant-mobile-number" name="applicant-mobile-number" type="text" maxlength="32" value="{$addressValues['applicant-mobile-number']}"></td>
        </tr>
    {/if}

    {if $showFields['applicant-email-address']}
            <tr>
            <td>{l s='E-Mail address' mod='resursbankpayment'}</td>
            <td><input id="applicant-email-address" name="applicant-email-address" type="text"></td>
        </tr>
    {/if
        }

    {if $showFields['finish-button']}
    <tr>
        <td colspan="2">
            <button type="submit" class="button btn btn-default button-medium">
                <span>{l s='Book payment' mod='resursbankpayment'}<i class="icon-chevron-right right"></i></span>
            </button>
        </td>
    </tr>
    {/if}
</table>
</form>


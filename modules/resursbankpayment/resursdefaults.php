<?php

// Define where to find stubs for API
define('RB_API_PATH', _PS_MODULE_DIR_ . "resursbankpayment");

class ResursBankLoader
{
    private $vars = null;

    public function getResursVars()
    {
        $resursVars = array(
            'RESURS_ENVIRONMENT' => 'test',
            'RESURS_ENVIRONMENT_PRODUCTION' => 'https://ecommerce.resurs.com/ws/V4/',
            'RESURS_ENVIRONMENT_TEST' => 'https://test.resurs.com/ecommerce-test/ws/V4/',
            'RESURS_PAYMENT_METHODS' => serialize(array()),
            'RESURS_PAYMENT_METHODS_CONFIGURATION' => serialize(array()),
            'RESURS_REPRESENTATIVE_ID' => '',
            'RESURS_REPRESENTATIVE_PASSWORD' => '',
            'RESURS_CALLBACK_URL' => '',
            'RESURS_CALLBACK_DIGEST' => serialize(array()),
            'RESURS_STATE_PENDING' => '',
            'RESURS_STATE_PAID' => ''
        );
        $resursGetMulti = array();
        foreach ($resursVars as $resursVar => $resursValue) {$resursGetMulti[] = $resursVar;}
        $conf = Configuration::getMultiple($resursGetMulti);
        return array('defaults' => $resursVars, 'conf' => $conf);
    }
    public function getCurrentConfig()
    {
        $tmpVars = $this->getResursVars();
        return $tmpVars['conf'];
    }
}


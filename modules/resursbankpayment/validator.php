<?php

include_once(dirname(__FILE__) . '/../../config/config.inc.php');
include_once(dirname(__FILE__) . '/../../init.php');
include_once(dirname(__FILE__) . '/rbapiloader.php');
include_once(dirname(__FILE__) . '/resursbankpayment.php');
include_once(_PS_MODULE_DIR_ . 'resursbankpayment/resursdefaults.php');

$RBLoader = new ResursBankLoader();
$resursVars = $RBLoader->getResursVars();
$config = $resursVars['conf'];

$url = Tools::getProtocol(Tools::usingSecureMode()) . Tools::getHttpHost(false, true) . __PS_BASE_URI__;
$moduleUrl = $url . "modules/resursbankpayment/";

/*if (!(int)$cookie->id_cart) {
    Tools::redirect($url . 'index.php?controller=order');
}*/

$requestCartId = intval(Tools::getValue("c"));
$cart = new Cart($requestCartId);
$cartId = (int)$cart->id;
$requestKey = Tools::getValue("k");
$orderId = Order::getOrderByCartId((int)($requestCartId));
$order = new Order($orderId);
$resurs = new ResursBankPayment();

if (!$cartId && (int)$order->id_cart > 0) {
    $cartId = $order->id_cart;
}

//if (isset($cartId) && md5($cartId . $cart->secure_key) == $requestKey) {
if (isset($cartId)) {
    $address = new Address(intval($cart->id_address_invoice));
    $customer = new Customer(intval($cart->id_customer));
    if ($address->id_state) {$state = new State(intval($address->id_state));}
    $orderId = Order::getOrderByCartId((int)($cartId));
    //$validator = $resurs->validateOrder($cart->id, $config['RESURS_STATE_PAID'], $amount, $resurs->displayName, $orderMessage, array(), (int)$currency_order->id, false, $customer->secure_key);

    $state = Tools::getValue("s");
    if ($state == "fail") {
        $history = new OrderHistory();
        $history->id_order = (int)$order->id;
        $history->changeIdOrderState(_PS_OS_CANCELED_, (int)($order->id));
        Tools::redirect($url . 'index.php?controller=order');
    } else {
        $history = new OrderHistory();
        $history->id_order = (int)$order->id;
        $history->changeIdOrderState((int)$config['RESURS_STATE_PAID'], (int)($order->id));
    }
    Tools::redirectLink(__PS_BASE_URI__ . 'index.php?controller=order-confirmation&id_cart=' . $cartId . '&id_module=' . $resurs->id . '&id_order=' . $id_order . '&key=' . $customer->secure_key);
}






exit;
<?php

/*
 * Resurs Bank Passthrough API - Simplify shopflow for Resurs Bank.
 * ecommerce support <ecommerce.support@resurs.se>
 *
 */

// Set up paths for autoloader. For user defined locations, use RB_API_PATH. If nothing is set up RB_CWD will be current cwd
if (defined('RB_API_PATH') && !defined('RB_CWD')) define('RB_CWD', RB_API_PATH);
if (!defined('RB_CWD')) define('RB_CWD', (($getcwd = getcwd()) ? $getcwd : '.'));

//if (file_exists(RB_CWD . '/developerservice-client/autoload.php')) {require RB_CWD . '/developerservice-client/autoload.php';}

class ResursBank {

    const ENVIRONMENT_PRODUCTION = 0;
    const ENVIRONMENT_TEST = 1;

    var $soapOptions = array(
            'exceptions'         => 1,
            'connection_timeout' => 60,
            'login'              => 'exshop',
            'password'           => 'gO9UaWH38D',
            'trace'              => 1
    );

    protected $version = "1.0.0";
    private $env_test = "https://test.resurs.com/ecommerce-test/ws/V4/";
    private $env_prod = "https://ecommerce.resurs.com/ws/V4/";
    private $hasinit = false;
    public $debug = false;

    public $environment = null;
    public $current_environment = self::ENVIRONMENT_TEST;
	// Web services
	public $configurationService = null;
	public $developerWebService = null;
	public $simplifiedShopFlowService = null;
	public $afterShopFlowService = null;
    public $shopFlowService = null;
	public $serviceReturn = null;
    public $lastError = null;

    /*
     * This API is set to primary use Resurs simplified shopflow. By default, this service is loaded automatically, together with the configurationservice which is used for
     * setting up callbacks, etc. If you need other services, like aftershop, you should add it when your API is loading like this for example:
     * $API->Include[] = 'AfterShopFlowService';
     *
     */
    public $Include = array('ConfigurationService', 'SimplifiedShopFlowService');
	
    public $username = null;
    public $password = null;
    public $HTTPS = true;         /* Always require SSL */


    /* BookPaymentSection */
    public $defaultUnitMeasure = "st";  // st/"styck" for sweden. Please change this from your plugin if you're not in Sweden.
    private $_paymentData = null;
    private $_paymentSpeclines = null;
    private $_specLineID = null;

    private $_paymentOrderData = null;
    private $_paymentAddress = null;
    private $_paymentDeliveryAddress = null;
    private $_paymentCustomer = null;
    private $_paymentExtendedCustomer = null;
    private $_paymentCardData = null;

    private $cardDataCardNumber = null;
    private $cardDataUseAmount = false;
    private $cardDataOwnAmount = null;

    //private $_paymentCart = null;

    /**
     * Initializer for WSDL before calling services. Decides what username and environment to use. Default is always test.
     *
     * @throws Exception
     *
     */

    private function initWsdl()
    {
        $this->testWrappers();
        /*
         * Make sure that the correct webservice is loading first. The topmost service has the highest priority and will not be overwritten once loaded.
         * For example, if ShopFlowService is loaded before the SimplifiedShopFlowService, you won't be able to use the SimplifiedShopFlowService at all.
         *
         */
        $apiFileLoads = 0;
        $apiLoads = 0;
        if (in_array('simplifiedshopflowservice', array_map("strtolower", $this->Include)) && file_exists(RB_CWD . '/simplifiedshopflowservice-client/Resurs_SimplifiedShopFlowService.php')) {require RB_CWD . '/simplifiedshopflowservice-client/Resurs_SimplifiedShopFlowService.php';$apiFileLoads++;}
        if (in_array('configurationservice', array_map("strtolower", $this->Include)) && file_exists(RB_CWD . '/configurationservice-client/Resurs_ConfigurationService.php')) {require RB_CWD . '/configurationservice-client/Resurs_ConfigurationService.php';$apiFileLoads++;}
        if (in_array('aftershopflowservice', array_map("strtolower", $this->Include)) && file_exists(RB_CWD . '/aftershopflowservice-client/Resurs_AfterShopFlowService.php')) {require RB_CWD . '/aftershopflowservice-client/Resurs_AfterShopFlowService.php';$apiFileLoads++;}
        if (in_array('shopflowservice', array_map("strtolower", $this->Include)) && file_exists(RB_CWD . '/shopflowservice-client/Resurs_ShopFlowService.php')) {require RB_CWD . '/shopflowservice-client/Resurs_ShopFlowService.php';$apiFileLoads++;}
        if (!$apiFileLoads) {
            throw new Exception("No service classes found");
        }

        // Requiring that SSL is available on the current server, will throw an exception if no HTTPS-wrapper is found.
        if (is_null($this->current_environment)) $this->current_environment = self::ENVIRONMENT_TEST;
        if ($this->username != null) $this->soapOptions['login'] = $this->username;
        if ($this->password != null) $this->soapOptions['password'] = $this->password;
        if ($this->current_environment == self::ENVIRONMENT_TEST){
            $this->environment = $this->env_test;
        }
        else{
            $this->environment = $this->env_prod;
        }

        try {
            if (class_exists('Resurs_SimplifiedShopFlowService')) {$this->simplifiedShopFlowService = new Resurs_SimplifiedShopFlowService($this->soapOptions, $this->environment . "SimplifiedShopFlowService?wsdl");$apiLoads++;}
            if (class_exists('Resurs_ConfigurationService')) {$this->configurationService = new Resurs_ConfigurationService($this->soapOptions, $this->environment . "ConfigurationService?wsdl");$apiLoads++;}
			if (class_exists('Resurs_AfterShopFlowService')) {$this->afterShopFlowService = new Resurs_AfterShopFlowService($this->soapOptions, $this->environment . "AfterShopFlowService?wsdl");$apiLoads++;}
            if (class_exists('Resurs_ShopFlowService')) {$this->shopFlowService = new Resurs_ShopFlowService($this->soapOptions, $this->environment . "ShopFlowService?wsdl");$apiLoads++;}
			//if (class_exists('DeveloperWebService')) {$this->developerWebService = new DeveloperWebService($this->soapOptions, $this->environment . "DeveloperWebService?wsdl");}
        }
        catch (Exception $e) {
            throw new Exception($e->getMessage);
        }
        if (!$apiFileLoads) {
            throw new Exception("No service loaded");
        }

        $this->hasinit = true;
        return;
    }

    /**
     * Check HTTPS-requirements
     * @throws Exception
     *
     */

    private function testWrappers()
    {
        if ($this->HTTPS === true) {
            if (!in_array('https', @stream_get_wrappers())) {
                throw new Exception("HTTPS wrapper can not be found");
            }
        }
    }

    /**
     * Setting up defaults for the passthroughAPI
     * @param null $login
     * @param null $password
     * @throws Exception
     *
     */

    function __construct($login = null, $password = null)
    {
		if (!class_exists('ReflectionClass')) {throw new Exception("ReflectionClass can not be found");}
        $this->soapOptions['cache_wsdl'] = (defined('WSDL_CACHE_BOTH') ? WSDL_CACHE_BOTH : true);
        $this->soapOptions['ssl_method'] = (defined('SOAP_SSL_METHOD_TLS') ? SOAP_SSL_METHOD_TLS : false);
        if (!is_null($login)) {
            $this->soapOptions['login'] = $login;
            $this->username = $login; // For use with initwsdl
        }
        if (!is_null($password)) {
            $this->soapOptions['password'] = $password;
            $this->password = $password; // For use with initwsdl
        }
    }

    /**
     * ResponseObjectArrayParser. Translates a return-object to a clean array
     * @param null $returnObject
     * @return array
     *
     */

    public function parseReturn($returnObject = null)
    {
        $hasGet = false;
        if (is_array($returnObject))
        {
            $parsedArray = array();
            foreach ($returnObject as $arrayName => $objectArray)
            {
                $classMethods = get_class_methods($objectArray);
                if (is_array($classMethods)) {
                    foreach ($classMethods as $classMethodId => $classMethod) {
                        if (preg_match("/^get/i", $classMethod)) {
                            $hasGet = true;
                            $field = lcfirst(preg_replace("/^get/i", '', $classMethod));
                            $objectContent = $objectArray->$classMethod();
                            if (is_array($objectContent)) {
                                $parsedArray[$arrayName][$field] = $this->parseReturn($objectContent);
                            } else {
                                $parsedArray[$arrayName][$field] = $objectContent;
                            }
                        }
                    }
                }
            }
            /* Failver test */
            if (!$hasGet && !count($parsedArray)) {
                return $this->objectsIntoArray($returnObject);
            }
            return $parsedArray;
        }
    }

    /** Convert objects to array data
     * @param $arrObjData
     * @param array $arrSkipIndices
     * @return array
     */
    function objectsIntoArray($arrObjData, $arrSkipIndices = array())
    {
        $arrData = array();
        // if input is object, convert into array
        if (is_object($arrObjData)) {$arrObjData = get_object_vars($arrObjData);}
        if (is_array($arrObjData))
        {
            foreach ($arrObjData as $index => $value)
            {
                if (is_object($value) || is_array($value))
                {
                    $value = $this->objectsIntoArray($value, $arrSkipIndices); // recursive call
                }
                if (@in_array($index, $arrSkipIndices))
                {
                    continue;
                }
                $arrData[$index] = $value;
            }
        }
        return $arrData;
    }


    /*
     * Unknown calls passed through __call(), checking if they exists in the stubs. If they do, we'll make a proper call to it.
     * Also makes sure a connection to Resurs Bank is ready via initWsdl()
     *
     * This method also takes control of responses. Default return value is the object returned from stubs.
     * The not so default value is a conversion from the response-object to a pure array. Adding "Array" to the called method will do this.
     *
     * Examples:
     *
     *      Get payment methods
     *          $rbapi->getPaymentMethods() returns the repsonseobject from Resurs Bank data without modifications (it keeps the object intact)
     *          $rbapi->getPaymentMethodsArray() returns the responseobject from Resurs Bank as an array
     *
     * Register a callback (returning a boolean depending on the registration result):
     *
     *    $digestArray = array(
     *       'digestSalt' => 'digestSaltString',
     *       'digestParameters' => array('paymentId', 'result')
     *    );
     *   $callbackSetResult = $rbapi->setCallback(ResursCallbackTypes::AUTOMATIC_FRAUD_CONTROL, "http://localhost/callbacks/digest/{digest}/paymentId/{paymentId}", $digestArray);
     *
     */

    /**
     * Methods Passthrough
     *
     * @param null $func
     * @param array $args
     * @return array|null
     * @throws Exception
     *
     */
    public function __call($func = null, $args = array())
    {
        $returnObject = null;
		$this->serviceReturn = null;
        $returnAsArray = false;
        $classfunc = null;
		$funcArgs = null;
        $returnContent = null;
		//if (isset($args[0]) && is_array($args[0])) {}
        $classfunc = "resurs_" . $func;
        if (preg_match("/Array$/", $func))
        {
            $func = preg_replace("/Array$/", '', $func);
            $classfunc = preg_replace("/Array$/", '', $classfunc);
            $returnAsArray = true;
        }
        try {
            if (!$this->hasinit) {
                // if initWsdl is not public
                try {
                    $this->initWsdl();
                }
                catch (Exception $e)
                {
                    throw new Exception($e);
                }
            }
        }
        catch (Exception $initWsdlException) { throw new Exception($initWsdlException); }
        try {
			$reflection = new ReflectionClass($classfunc);
			$instance = $reflection->newInstanceArgs($args);
			// Check availability, fetch and stop on first match
            if (!isset($returnObject) && in_array($func, get_class_methods("Resurs_SimplifiedShopFlowService"))) {$this->serviceReturn = "SimplifiedShopFlowService";$returnObject = $this->simplifiedShopFlowService->$func($instance);}
            if (!isset($returnObject) && in_array($func, get_class_methods("Resurs_ConfigurationService"))) {$this->serviceReturn = "ConfigurationService";$returnObject = $this->configurationService->$func($instance);}
            if (!isset($returnObject) && in_array($func, get_class_methods("Resurs_AfterShopFlowService"))) {$this->serviceReturn = "AfterShopFlowService";$returnObject = $this->afterShopFlowService->$func($instance);}
			if (!isset($returnObject) && in_array($func, get_class_methods("Resurs_ShopFlowService"))) {$this->serviceReturn = "ShopFlowService";$returnObject = $this->shopFlowService->$func($instance);}
            //if (!isset($returnObject) && in_array($func, get_class_methods("DeveloperService"))) {$this->serviceReturn = "DeveloperService";$returnObject = $this->developerService->$func($instance);}
        }
        catch (Exception $e) {throw new Exception($e);}
        try {
            //if (method_exists($returnObject, "getReturn")) {
            if (isset($returnObject) && !empty($returnObject) && isset($returnObject->return) && !empty($returnObject->return)) {
                //$returnContent = $returnObject->getReturn();
                $returnContent = $returnObject->return;
                if ($returnAsArray) {
                    return $this->parseReturn($returnContent);
                }
            }
            else {
                if (!empty($returnObject) && $returnAsArray)
                {
                    return array();
                }
            }
            return $returnContent;
        }
        catch (Exception $returnObjectException) {}
        if ($returnAsArray) {return $this->parseReturn($returnObject);}
        return $returnObject;
    }


    /**
     * Simplifed callback registrator. Also handles re-registering of callbacks in case of already found in system.
     * @param null $callbackType
     * @param null $callbackUriTemplate
     * @param array $callbackDigest
     * @param null $basicAuthUserName
     * @param null $basicAuthPassword
     * @return bool
     * @throws Exception
     *
     */

    public function setCallback($callbackType = null, $callbackUriTemplate = null, $callbackDigest = array(), $basicAuthUserName = null, $basicAuthPassword = null)
    {
        if (!$this->hasinit) {
            $this->initWsdl();
        }

        if (!is_array($callbackDigest) || empty($callbackDigest)) {
            throw new Exception("Insufficient callback digest data for callback registration");
        }
        $renderCallback = array();
        $digestParameters = array();
        if ($callbackType == ResursCallbackTypes::ANNULMENT) {
            $renderCallback['eventType'] = "ANNULMENT";
        }
        if ($callbackType == ResursCallbackTypes::AUTOMATIC_FRAUD_CONTROL) {
            $renderCallback['eventType'] = "AUTOMATIC_FRAUD_CONTROL";
        }
        if ($callbackType == ResursCallbackTypes::UNFREEZE) {
            $renderCallback['eventType'] = "UNFREEZE";
        }
        if ($callbackType == ResursCallbackTypes::FINALIZATION) {
            $renderCallback['eventType'] = "FINALIZATION";
        }
        if ($callbackType == ResursCallbackTypes::TEST) {
            $renderCallback['eventType'] = "TEST";
        }
        if (count($renderCallback) && $renderCallback['eventType'] != "" && $callbackUriTemplate != "") {
            $regCallBackResult = false;
            $registerCallbackClass = new resurs_registerEventCallback($renderCallback['eventType'], $callbackUriTemplate);
            $digestAlgorithm = resurs_digestAlgorithm::SHA1;

            /* Look for parameters in the request. Algorithms are set to SHA1 by default. If no digestsalts are set, we will throw you an exception, since empty salts is normally not a good idea */
            if (is_array($callbackDigest)) {
                if (isset($callbackDigest['digestAlgorithm']) && strtolower($callbackDigest['digestAlgorithm']) != "sha1" && strtolower($callbackDigest['digestAlgorithm']) != "md5") {
                    $callbackDigest['digestAlgorithm'] = "sha1";
                } elseif (!isset($callbackDigest['digestAlgorithm'])) {
                    $callbackDigest['digestAlgorithm'] = "sha1";
                }
                /* If requested algorithm is not sha1, use md5 as the other option. */
                if ($callbackDigest['digestAlgorithm'] != "sha1") {
                    $digestAlgorithm = digestAlgorithm::MD5;
                }

                /* Start collect the parameters needed for the callback. */
                $parameterArray = array();
                if (isset($callbackDigest['digestParameters']) && is_array($callbackDigest['digestParameters'])) {
                    foreach ($callbackDigest['digestParameters'] as $parameter) {
                        array_push($parameterArray, $parameter);
                    }
                }

                /* Make sure there is a saltkey. */
                if (isset($callbackDigest['digestSalt']) && !empty($callbackDigest['digestSalt'])) {
                    $digestParameters['digestSalt'] = $callbackDigest['digestSalt'];
                } else {
                    throw new Exception("No salt key for digest found");
                }
                $digestParameters['digestParameters'] = (is_array($parameterArray) ? $parameterArray : array());
            }
            /* Generate a digest configuration for the services. */
            $digestConfiguration = new resurs_digestConfiguration($digestAlgorithm, $digestParameters['digestParameters']);
            $digestConfiguration->digestSalt = $digestParameters['digestSalt'];

            /* Old stub call */
            // $digestConfiguration->setDigestSalt($digestParameters['digestSalt']);

            /* Unregister any old callbacks if found. */
            try {
                $this->unregisterEventCallback($renderCallback['eventType']);
            } catch (Exception $e) {
            }   // Unregister old callback first
            if (!empty($basicAuthUserName)) {
                $registerCallbackClass->setBasicAuthUserName($basicAuthUserName);
            }
            if (!empty($basicAuthPassword)) {
                $registerCallbackClass->setBasicAuthPassword($basicAuthPassword);
            }

            /* Prepare for the primary digestive data. */
            /* Old stub call */
            //$registerCallbackClass->setDigestConfiguration($digestConfiguration);
            $registerCallbackClass->digestConfiguration = $digestConfiguration;
            try {
                /* And register the rendered callback at the service. Make sure that configurationService is really there before doing this. */
                $regCallBackResult = $this->configurationService->registerEventCallback($registerCallbackClass);
            } catch (Exception $rbCallbackEx) {
                /* Set up a silent failover, and return false if the callback registration failed. Set the error into the lastError-variable. */
                $regCallBackResult = false;
                $this->lastError = $rbCallbackEx->getMessage();
            }
            return $regCallBackResult;
        } else {
            throw new Exception("Insufficient data for callback registration");
        }
    }

    /**
     * List payment methods
     * @return mixed
     * @throws Exception
     */
    public function paymentMethodList() {
        if (!$this->hasinit) {$this->initWsdl();}
        return $this->simplifiedShopFlowService->getPaymentMethods();
    }

    /**
     * Prepare a payment
     * @param $paymentMethodId
     * @param array $paymentDataArray
     * @throws Exception
     */
    public function updatePaymentdata($paymentMethodId, $paymentDataArray = array()) {
        if (!$this->hasinit) {$this->initWsdl();}
        if (!is_object($this->_paymentData)) { $this->_paymentData = new resurs_paymentData($paymentMethodId); }
        $this->_paymentData->preferredId = isset($paymentDataArray['preferredId']) && !empty($paymentDataArray['preferredId']) ? $paymentDataArray['preferredId'] : null;
        $this->_paymentData->paymentMethodId = $paymentMethodId;
        $this->_paymentData->customerIpAddress = (isset($paymentDataArray['customerIpAddress']) && !empty($paymentDataArray['customerIpAddress']) ? $paymentDataArray['customerIpAddress'] : (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1'));
        $this->_paymentData->waitForFraudControl = isset($paymentDataArray['waitForFraudControl']) && !empty($paymentDataArray['waitForFraudControl']) ? $paymentDataArray['waitForFraudControl'] : false;
        $this->_paymentData->annulIfFrozen = isset($paymentDataArray['annulIfFrozen']) && !empty($paymentDataArray['annulIfFrozen']) ? $paymentDataArray['annulIfFrozen'] : false;
        $this->_paymentData->finalizeIfBooked = isset($paymentDataArray['finalizeIfBooked']) && !empty($paymentDataArray['finalizeIfBooked']) ? $paymentDataArray['finalizeIfBooked'] : false;
    }

    /**
     * Update cart
     * @param array $speclineArray
     * @throws Exception
     */
    public function updateCart($speclineArray = array())
    {
        $this->initWsdl();

        $realSpecArray = array();
        if (isset($speclineArray['artNo'])) {
            // If this require parameter is found first in the array, it's a single specrow.
            // In that case, push it out to be a multiple.
            array_push($realSpecArray, $speclineArray);
        } else {
            $realSpecArray = $speclineArray;
        }

        // Handle the specrows as they were many.
        foreach ($realSpecArray as $specIndex => $speclineArray) {
            $quantity = (isset($speclineArray['quantity']) && !empty($speclineArray['quantity']) ? $speclineArray['quantity'] : 1);
            $unitAmountWithoutVat = (is_numeric(floatval($speclineArray['unitAmountWithoutVat'])) ? $speclineArray['unitAmountWithoutVat'] : 0);
            $vatPct = (isset($speclineArray['vatPct']) && !empty($speclineArray['vatPct']) ? $speclineArray['vatPct'] : 0);
            //$totalVatAmountInternal = round(($unitAmountWithoutVat*$quantity)-($unitAmountWithoutVat/(1+($vatPct/100))*$quantity),2);
            $totalVatAmountInternal = ($unitAmountWithoutVat * ($vatPct / 100)) * $quantity;
            $totalAmountInclTax = round(($unitAmountWithoutVat * $quantity) + $totalVatAmountInternal, 2);

            if (isset($speclineArray['totalVatAmount']) && !empty($speclineArray['totalVatAmount'])) {
                $totalVatAmount = $speclineArray['totalVatAmount'];
                $totalAmountInclTax = $unitAmountWithoutVat + $totalVatAmount;
            } else {
                $totalVatAmount = $totalVatAmountInternal;
            }

            if (!class_exists('resurs_specLine')) {
                throw new Exception("Class specLine does not exist");
                return;
            }
            $this->_specLineID++;
            $this->_paymentSpeclines[] = new resurs_specLine(
                $this->_specLineID,
                $speclineArray['artNo'],
                $speclineArray['description'],
                $speclineArray['quantity'],
                (isset($speclineArray['unitMeasure']) && !empty($speclineArray['unitMeasure']) ? $speclineArray['unitMeasure'] : $this->defaultUnitMeasure),
                $unitAmountWithoutVat,
                $vatPct,
                $totalVatAmount,
                $totalAmountInclTax
            );
        }
    }

    /**
     * Update payment specs and prepeare specrows
     * @param array $specLineArray
     * @throws Exception
     */
    public function updatePaymentSpec($specLineArray = array()) {
        if (!$this->hasinit) {$this->initWsdl();}
        $totalAmount = 0;
        $totalVatAmount = 0;
        if (is_array($specLineArray) && count($specLineArray)) {
            foreach ($specLineArray as $specRow => $specRowArray) {
                $totalAmount += (isset($specRowArray->totalAmount) ? $specRowArray->totalAmount : 0);
                $totalVatAmount += (isset($specRowArray->totalVatAmount) ? $specRowArray->totalVatAmount : 0);
            }
        }
        $this->_paymentOrderData = new resurs_paymentSpec($specLineArray, $totalAmount, 0);
        $this->_paymentOrderData->totalVatAmount = floatval($totalVatAmount);
    }

    /**
     * Prepare customer address data
     * @param array $addressArray
     * @param array $customerArray
     * @throws Exception
     */
    public function updateAddress($addressArray = array(), $customerArray = array())
    {
        if (!$this->hasinit) { $this->initWsdl(); }
        $resursDeliveryAddress = null;

        if (isset($addressArray['address'])) {
            $address = new resurs_address($addressArray['address']['fullName'], $addressArray['address']['firstName'], $addressArray['address']['lastName'], (isset($addressArray['address']['addressRow1']) ? $addressArray['address']['addressRow1'] : null), (isset($addressArray['address']['addressRow2']) ? $addressArray['address']['addressRow2'] : null), $addressArray['address']['postalArea'], $addressArray['address']['postalCode'], $addressArray['address']['country']);
            if (isset($addressArray['deliveryAddress'])) {
                $resursDeliveryAddress = new resurs_address($addressArray['deliveryAddress']['fullName'], $addressArray['deliveryAddress']['firstName'], $addressArray['deliveryAddress']['lastName'], (isset($addressArray['deliveryAddress']['addressRow1']) ? $addressArray['deliveryAddress']['addressRow1'] : null), (isset($addressArray['deliveryAddress']['addressRow2']) ? $addressArray['deliveryAddress']['addressRow2'] : null), $addressArray['deliveryAddress']['postalArea'], $addressArray['deliveryAddress']['postalCode'], $addressArray['deliveryAddress']['country']);
            }
        } else {
            $address = new resurs_address($addressArray['fullName'], $addressArray['firstName'], $addressArray['lastName'], (isset($addressArray['addressRow1']) ? $addressArray['addressRow1'] : null), (isset($addressArray['addressRow2']) ? $addressArray['addressRow2'] : null), $addressArray['postalArea'], $addressArray['postalCode'], $addressArray['country']);
        }
        $customer = new resurs_customer($customerArray['governmentId'], $address, $customerArray['phone'], $customerArray['email'], $customerArray['type']);
        $this->_paymentAddress = $address;
        if (!empty($resursDeliveryAddress) || $customerArray['type'] == "LEGAL") {
            if (isset($resursDeliveryAddress) && is_array($resursDeliveryAddress)) {$this->_paymentDeliveryAddress = $resursDeliveryAddress;}
            $extendedCustomer = new resurs_extendedCustomer($customerArray['governmentId'], $resursDeliveryAddress, $customerArray['phone'], $customerArray['email'], $customerArray['type']);
            $this->_paymentExtendedCustomer = $extendedCustomer;
            if ($customerArray['type'] == "LEGAL") { $extendedCustomer->contactGovernmentId = $customerArray['contactGovernmentId']; }
            if (!empty($customerArray['cellPhone'])) { $extendedCustomer->cellPhone = $customerArray['cellPhone']; }
        }
        $this->_paymentCustomer = $customer;
        if (isset($extendedCustomer)) {
            $extendedCustomer->address = $address;
            $this->_paymentCustomer = $extendedCustomer;
        }
    }

    /**
     * Internal handler for carddata
     * @throws Exception
     */
    private function updateCardData() {
        $amount = null;
        $this->_paymentCardData = new resurs_cardData();
        if (!isset($this->cardDataCardNumber)) {
            if ($this->cardDataUseAmount && $this->cardDataOwnAmount) {
                $this->_paymentCardData->amount = $this->cardDataOwnAmount;
            } else {
                $this->_paymentCardData->amount = $this->_paymentOrderData->totalAmount;
            }
        } else {
            if (isset($this->cardDataCardNumber) && !empty($this->cardDataCardNumber)) {
                $this->_paymentCardData->cardNumber = $this->cardDataCardNumber;
            }
        }
        if (!empty($this->cardDataCardNumber) && !empty($this->cardDataUseAmount)) {
            throw new Exception("Card number and amount can not be set at the same time");
        }
        return $this->_paymentCardData;
    }

    /**
     * Prepare API for cards. Make sure only one of the parameters are used. Cardnumber cannot be combinad with amount.
     *
     * @param null $cardNumber
     * @param bool|false $useAmount     Set to true when using new cards
     * @param bool|false $setOwnAmount  If customer applies for a new card specify the credit amount that is applied for. If $setOwnAmount is not null, this amount will be used instead of the specrow data
     */
    public function prepareCardData($cardNumber = null, $useAmount = false, $setOwnAmount = null) {
        if (!is_null($cardNumber)) {
            if (is_numeric($cardNumber)) {
                $this->cardDataCardNumber = $cardNumber;
            } else {
                throw new Exception("Card number must be numeric");
            }
        }
        if ($useAmount) {
            $this->cardDataUseAmount = true;
            if (!is_null($setOwnAmount) && is_numeric($setOwnAmount)) {$this->cardDataOwnAmount = $setOwnAmount;}
        }
    }


    /*
            Example / Required for bookPayment

            $bookData['specLine'] = array(
                'artNo' => 'art_0_1',
                'description' => 'Artikel nummer 1',
                'quantity' => 1,
                'unitAmountWithoutVat' => 500,
                'vatPct' => 25
            );
            $bookData['address'] = array(
                'fullName' => 'Test Testsson',
                'firstName' => 'Test',
                'lastName' => 'Testsson',
                'addressRow1' => 'Testgatan 1',
                'postalArea' => 'Testort',
                'postalCode' => '12121',
                'country' => 'SE'
            );
            $bookData['customer'] = array(
                'governmentId' => '198305147715',
                'phone' => '0101010101',
                'email' => 'test@test.com',
                'type' => 'NATURAL'
            );
            $bookData['signing'] = array(
                'successUrl' => 'http://www.aftonbladet.se',
                'failUrl' => 'http://www.expressen.se',
                'forceSigning' => false
            );
            $rbapi->bookPayment("112", $bookData);

    */
    public function bookPayment($paymentMethodId, $bookData = array()) {
        if (empty($paymentMethodId)) {
            return;
        }
        if (!$this->hasinit) {$this->initWsdl();}
        $this->updatePaymentdata($paymentMethodId, isset($bookData['paymentData']) && is_array($bookData['paymentData']) && count($bookData['paymentData']) ? $bookData['paymentData'] : array());
        $this->updateCart(isset($bookData['specLine']) ? $bookData['specLine'] : array());
        $this->updatePaymentSpec($this->_paymentSpeclines);
        if (isset($bookData['deliveryAddress'])) {
            $addressArray = array(
                'address' => $bookData['address'],
                'deliveryAddress' => $bookData['deliveryAddress']
            );
            $this->updateAddress(isset($addressArray) ? $addressArray : array(), isset($bookData['customer']) ? $bookData['customer'] : array());
        } else {
            $this->updateAddress(isset($bookData['address']) ? $bookData['address'] : array(), isset($bookData['customer']) ? $bookData['customer'] : array());
        }
        $bookPaymentInit = new resurs_bookPayment($this->_paymentData, $this->_paymentOrderData, $this->_paymentCustomer);

        if (!empty($this->cardDataCardNumber) || $this->cardDataUseAmount) {
            $bookPaymentInit->card = $this->updateCardData();
        }
        if (!empty($this->_paymentDeliveryAddress) && is_object($this->_paymentDeliveryAddress)) {
            $bookPaymentInit->customer->deliveryAddress = $this->_paymentDeliveryAddress;
        }
        $bookPaymentInit->signing = $bookData['signing'];
        $bookPaymentResult = $this->simplifiedShopFlowService->bookPayment($bookPaymentInit);
        return $bookPaymentResult;
    }
}


abstract class ResursCallbackTypes {
    const UNFREEZE = 1;
    const ANNULMENT = 2;
    const AUTOMATIC_FRAUD_CONTROL = 3;
    const FINALIZATION = 4;
    const TEST = 5;
}
<?php

/* E-commerce Callback Handling */

include_once(dirname(__FILE__) . '/../../config/config.inc.php');
include_once(dirname(__FILE__) . '/../../init.php');
include_once(dirname(__FILE__) . '/rbapiloader.php');
include_once(dirname(__FILE__) . '/resursbankpayment.php');
include_once(_PS_MODULE_DIR_ . 'resursbankpayment/resursdefaults.php');

$RBLoader = new ResursBankLoader();
$resursVars = $RBLoader->getResursVars();
$config = $resursVars['conf'];

// If there is a cookie, is this really a callback from Resurs?
// if (isset($cookie) && $cookie->id_customer) {}

/*
 * Example URL
 * http://store.example.com/modules/resursbankpayment/resurscallback.php?event=ANNULMENT&digest={digest}&paymentId={paymentId}
 *
 */

$event = strtoupper(Tools::getValue('event'));
$rb_digest = Tools::getValue('digest');
$paymentId = Tools::getValue('paymentId');
$result = Tools::getValue('result');

$RBLoader = new ResursBankLoader();
$RBPlugin = new ResursBankPayment();
$resursVars = $RBLoader->getResursVars();
$config = $resursVars['conf'];
$callbackInfo = unserialize($config['RESURS_CALLBACK_DIGEST']);
$currentSalt = isset($callbackInfo[$event]) && isset($callbackInfo[$event]['salt']) ? $callbackInfo[$event]['salt'] : null;

/* Default digest */
$my_digest = strtoupper(sha1(strtoupper($paymentId) . $currentSalt));
$rbStates = array(
    'paid' => $config['RESURS_STATE_PAID'],
    'pending' => $config['RESURS_STATE_PENDING']
);

if (!is_null($currentSalt) && !empty($paymentId)) {

    try {

        $order = new Order($paymentId);
        $currentState = $order->current_state;
        if ($order->id != "") {
            if ($event == "AUTOMATIC_FRAUD_CONTROL") {
                $my_digest = strtoupper(sha1(strtoupper($paymentId) . strtoupper($result) . $currentSalt));
                if ($rb_digest == $my_digest) {
                    $history = new OrderHistory();
                    $history->id_order = (int)$paymentId;
                    if ($result == "THAWED") {
                        $history->changeIdOrderState((int)$config['RESURS_STATE_PAID'], (int)($paymentId));
                        $history->addWithemail(true);
                        header("HTTP/1.0 204 Thawed");
                    } elseif ($result == "FROZEN") {
                        $history->changeIdOrderState((int)$config['RESURS_STATE_PENDING'], (int)($paymentId));
                        header("HTTP/1.0 204 Frozen");
                    } else {
                        header("HTTP/1.0 403 Unknown result " . $result);
                    }
                } else {
                    header("HTTP/1.0 403 Digest failure");
                }
            } elseif ($event == "UNFREEZE") {
                $history = new OrderHistory();
                $history->id_order = (int)$paymentId;
                $history->changeIdOrderState((int)$config['RESURS_STATE_PAID'], (int)($paymentId));
                header("HTTP/1.0 204 Thawed");
            } elseif ($event == "ANNULMENT") {
                $history = new OrderHistory();
                $history->id_order = (int)$paymentId;
                $history->changeIdOrderState((int)_PS_OS_CANCELED_, (int)($paymentId));
                header("HTTP/1.0 204 Annulled");
            } else {
                header("HTTP/1.0 403 Unsupported method " . $event);
            }
        } else {
            header("HTTP/1.0 403 Not a valid order");
        }

    }
    catch (Exception $orderStateException) {
        header("HTTP/1.0 403 " . $orderStateException->getMessage());
        echo $orderStateException->getMessage();
    }

} else {
    header("HTTP/1.0 403 Salt or payment id not complete");
}





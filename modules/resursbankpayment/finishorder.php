<?php

include_once(dirname(__FILE__) . '/../../config/config.inc.php');
include_once(dirname(__FILE__) . '/../../init.php');
include_once(dirname(__FILE__) . '/rbapiloader.php');
include_once(dirname(__FILE__) . '/resursbankpayment.php');
include_once(_PS_MODULE_DIR_ . 'resursbankpayment/resursdefaults.php');


function getSpeclineByCart($cart)
{
    $specRowArray = array();
    foreach ($cart->getProducts() as $cartProduct) {
        $specRowArray[] = array(
            'artNo' => $cartProduct['id_product'],
            'description' => (isset($cartProduct['name']) ? $cartProduct['name'] : "") . (isset($cartProduct['attributes_small']) ? $cartProduct['attributes_small'] : ''),
            'quantity' => $cartProduct['cart_quantity'],
            'unitAmountWithoutVat' => round((isset($cartProduct['price']) ? $cartProduct['price'] : 0), 2),
            'vatPct' => ($cartProduct['rate'])
        );
    }
    return $specRowArray;
}
function getSpeclineByOwn($specRowArray, $artNo, $description, $quantity, $unitAmountWithoutVat, $vatPct) {
    $specRowArray[] = array(
        'artNo' => $artNo,
        'description' => $description,
        'quantity' => $quantity,
        'unitAmountWithoutVat' => round($unitAmountWithoutVat, 2),
        'vatPct' => ($vatPct)
    );
    return $specRowArray;
}


$RBLoader = new ResursBankLoader();
$RBPlugin = new ResursBankPayment();

$resursVars = $RBLoader->getResursVars();
$config = $resursVars['conf'];
$methods = @unserialize($config['RESURS_PAYMENT_METHODS']);
// Do fail if empty, by returning to order page
if (!is_array($methods)) { Tools::redirect($url . 'index.php?controller=order'); }

$resurs = new ResursBankPayment();

if ($config['RESURS_REPRESENTATIVE_ID'] != "") {
    try {
        $rbapi = new ResursBank($config['RESURS_REPRESENTATIVE_ID'], $config['RESURS_REPRESENTATIVE_PASSWORD']);
    } catch (Exception $e) {
        resursAjaxError($e->getMessage());
        exit;
    }
    if (!is_object($rbapi)) {
        die("Failed to initialize RBObject");
    }
}

$url = Tools::getProtocol(Tools::usingSecureMode()) . Tools::getHttpHost(false, true) . __PS_BASE_URI__;
$moduleUrl = $url . "modules/resursbankpayment/";

$chosenMethod = Tools::getValue("resurs_payment_method");

// type, specificType (REVOLVING_CREDIT)
// type CARD (befintligt)

$methodType = null;
$methodSpecificType = null;
foreach ($methods as $method => $methodArray) {
    $id = $methodArray['id'];
    if ($id == $chosenMethod) {
        $methodType = $methodArray['type'];
        $methodSpecificType = isset($methodArray['specificType'])? $methodArray['specificType'] : null;
    }
}

if (empty($chosenMethod)) {
    Tools::redirect($url . 'index.php?controller=order&step=3');
}
if (!(int)$cookie->id_cart) {
    Tools::redirect($url . 'index.php?controller=order');
}
$cart = new Cart(intval($cookie->id_cart));
$cartId = $cart->id;
$address = new Address(intval($cart->id_address_invoice));
$deliveryAddress = new Address(intval($cart->id_address_delivery));
if ($address->id_state) {$state = new State(intval($address->id_state));}

$differentDelivery = false;
if (intval($cart->id_address_invoice) != intval($cart->id_address_delivery)) { $differentDelivery = true; }

$customer = new Customer(intval($cart->id_customer));
$currency_order = new Currency(intval($cart->id_currency));
$currencies_module = $resurs->getCurrency();
if (is_array($currencies_module)) {
    foreach ($currencies_module AS $some_currency_module) {
        if ($currency_order->iso_code == $some_currency_module['iso_code']) {
            $currency_module = $some_currency_module;
        }
    }
} else {
    $currency_module = $currencies_module;
}
if ($currency_order->id != $currency_module['id_currency']) {
    $cookie->id_currency = $currency_module['id_currency'];
    $cart->id_currency = $currency_module['id_currency'];
    $cart->update();
}

$amount = floatval($cart->getOrderTotal(true, Cart::BOTH));
$specRows = getSpeclineByCart($cart);
$validator = false;
if (is_array($specRows)) {

    $validator = $resurs->validateOrder($cart->id, $config['RESURS_STATE_PENDING'], $amount, $resurs->displayName, (isset($orderMessage) ? $orderMessage : "Order via Resurs Bank"), array(), (int)$currency_order->id, false, $customer->secure_key);

    if ($validator) {
        $currentOrderId = Order::getOrderByCartId(intval($cart->id));
        $cartDiscounts = $cart->getDiscounts();
        foreach ($cartDiscounts AS $cartDiscount) {
            $objDiscount = new Discount(intval($cartDiscount['id_discount']));
            $value = $objDiscount->getValue(sizeof($cartDiscounts), $cart->getOrderTotal(true, 1), $cart->getTotalShippingCost(), $cart->id);
            $specRows = getSpeclineByOwn($specRows, "discount", $cartDiscount['description'], $cartDiscount['quantity'], floatval("-" . $value), 0);
        }
        $total_shipping_wt = _PS_VERSION_ >= 1.5 ? floatval($cart->getTotalShippingCost()) : floatval($cart->getOrderShippingCost());
        if ($total_shipping_wt > 0) {
            $carrier = new Carrier($cart->id_carrier, $cart->id_lang);
            $carriertax = Tax::getCarrierTaxRate((int)$carrier->id, $cart->id_address_invoice);
            $carriertax_rate = $carriertax / 100;
            $forward_vat = 1 + $carriertax_rate;
            $total_shipping_wot = $total_shipping_wt / $forward_vat;
            $var = 0;
            $specRows = getSpeclineByOwn($specRows, (isset($carrier->name) ? $carrier->name : 'shipping'), (isset($carrier->name) ? $carrier->name : 'shipping'), 1, $total_shipping_wot, $carriertax);
        }

        $bookData['specLine'] = $specRows;
        $id_country = Country::getIdByName(null, $address->country);
        $id_d_country = Country::getIdByName(null, $deliveryAddress->country);
        $isoCountry = "";
        $isoDeliveryCountry = 0;
        if ($id_country > 0) { $isoCountry = Country::getIsoById($id_country); }
        if ($id_d_country > 0) { $isoDeliveryCountry = Country::getIsoById($id_d_country); }

        $bookData['paymentData'] = array(
            'preferredId' => $currentOrderId,
            'customerIpAddress' => $_SERVER['REMOTE_ADDR'],
            'waitForFraudControl' => false
        );

        $bookData['address'] = array(
            'fullName' => $address->firstname . " " . $address->lastname,
            'firstName' => $address->firstname,
            'lastName' => $address->lastname,
            'addressRow1' => $address->address1,
            'postalArea' => $address->city,
            'postalCode' => $address->postcode,
            'country' => $isoCountry
        );
        if ($differentDelivery) {
            $bookData['deliveryAddress'] = array(
                'fullName' => $deliveryAddress->firstname . " " . $deliveryAddress->lastname,
                'firstName' => $deliveryAddress->firstname,
                'lastName' => $deliveryAddress->lastname,
                'addressRow1' => $deliveryAddress->address1,
                'postalArea' => $deliveryAddress->city,
                'postalCode' => $deliveryAddress->postcode,
                'country' => $isoDeliveryCountry
            );
        }
        $customerType = Tools::getValue('resurs_type');
        $bookData['customer'] = array(
            'governmentId' => Tools::getValue('applicant-government-id'),
            'phone' => Tools::getValue('applicant-telephone-number'),
            'email' => Tools::getValue('applicant-email-address'),
            'type' => $customerType
        );
        if ($methodType == "REVOLVING_CREDIT" || $methodType == "CARD") {
            $bookData['customer'] = array(
                'governmentId' => Tools::getValue('applicant-government-id'),
                'type' => $customerType
            );
            if ($methodType == "REVOLVING_CREDIT") {
                $rbapi->prepareCardData(null, true);
            } else {
                $cardNumber = Tools::getValue('card-number');
                $rbapi->prepareCardData($cardNumber, false);
            }
        }

        if ($customerType == "LEGAL") {
            $bookData['customer']['contactGovernmentId'] = Tools::getValue('contact-government-id');
            // applicant full name?
        }
        if (Tools::getValue('applicant-mobile-number') != "") {
            $bookData['customer']['cellPhone'] = Tools::getValue('applicant-mobile-number');
        }
        //$hUrl = (isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $url;
        $hUrl = $moduleUrl;

        $cartId = $cart->id;
        $secureKey = $customer->secure_key;
        $bookData['signing'] = array(
            'successUrl' => $hUrl . 'validator.php?c=' . $cartId . "&k=" . md5($cartId . $secureKey),
            'failUrl' => $hUrl . 'validator.php?c=' . $cartId . "&k=" . md5($cartId . $secureKey) . "&s=fail",
            'forceSigning' => false
        );

        if (!empty($chosenMethod)) {
            $errors = array();
            try {
                $bookPaymentResponse = $rbapi->bookPayment($chosenMethod, $bookData);
                $bookPaymentResult = $bookPaymentResponse->return;
                $bookPaymentStatus = $bookPaymentResult->bookPaymentStatus;
                $id_order = Order::getOrderByCartId(intval($cart->id));
                if (is_object($bookPaymentResponse)) {
                    if ($bookPaymentStatus == "SIGNING") {
                        Tools::redirectLink($bookPaymentResult->signingUrl);
                    } elseif ($bookPaymentStatus == "BOOKED" || $bookPaymentResponse == "FINALIZED") {
                        $orderId = Order::getOrderByCartId((int)($cartId));
                        $order = new Order($orderId);
                        $history = new OrderHistory();
                        $history->id_order = (int)$order->id;
                        $history->changeIdOrderState((int)$config['RESURS_STATE_PAID'], (int)($order->id));
                        Tools::redirectLink(__PS_BASE_URI__ . 'index.php?controller=order-confirmation&id_cart=' . $cartId . '&id_module=' . $resurs->id . '&id_order=' . $id_order . '&key=' . $customer->secure_key);
                    } elseif ($bookPaymentStatus == "FROZEN") {
                        $orderId = Order::getOrderByCartId((int)($cartId));
                        $order = new Order($orderId);
                        $history = new OrderHistory();
                        $history->id_order = (int)$order->id;
                        $history->changeIdOrderState((int)$config['RESURS_STATE_PENDING'], (int)($order->id));
                        Tools::redirectLink(__PS_BASE_URI__ . 'index.php?controller=order-confirmation&id_cart=' . $cartId . '&id_module=' . $resurs->id . '&id_order=' . $id_order . '&key=' . $customer->secure_key);
                    } elseif ($bookPaymentStatus == "DENIED") {
                        $errors[] = "Your credit application has been rejected, contact Resurs Bank for more details.";
                    }
                } else {
                    $errors[] = "Message from Resurs Bank: An unknown error occured";
                }
            } catch (Exception $bookException) {
                $errors[] = "Message from Resurs Bank: " . $bookException->getMessage();
            }
            if (count($errors)) {
                $RBPlugin->HandleErrors($errors, 3);
            }
        }
    // End validator
    }
}


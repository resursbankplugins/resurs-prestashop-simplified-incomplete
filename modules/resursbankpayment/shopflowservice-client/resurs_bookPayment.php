<?php

if (!class_exists("resurs_bookPayment", false)) 
{
class resurs_bookPayment
{

    /**
     * @var id $paymentSessionId
     * @access public
     */
    public $paymentSessionId = null;

    /**
     * @var id $preferredPaymentId
     * @access public
     */
    public $preferredPaymentId = null;

    /**
     * @var boolean $waitForFraudControl
     * @access public
     */
    public $waitForFraudControl = null;

    /**
     * @var boolean $annulIfFrozen
     * @access public
     */
    public $annulIfFrozen = null;

    /**
     * @var mapEntry[] $metaData
     * @access public
     */
    public $metaData = null;

    /**
     * @var int $priority
     * @access public
     */
    public $priority = null;

    /**
     * @var id $storeId
     * @access public
     */
    public $storeId = null;

    /**
     * @param id $paymentSessionId
     * @param boolean $waitForFraudControl
     * @param boolean $annulIfFrozen
     * @param mapEntry[] $metaData
     * @param int $priority
     * @access public
     */
    public function __construct($paymentSessionId, $waitForFraudControl, $annulIfFrozen, $metaData, $priority)
    {
      $this->paymentSessionId = $paymentSessionId;
      $this->waitForFraudControl = $waitForFraudControl;
      $this->annulIfFrozen = $annulIfFrozen;
      $this->metaData = $metaData;
      $this->priority = $priority;
    }

}

}

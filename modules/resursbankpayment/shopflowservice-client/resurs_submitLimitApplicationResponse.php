<?php

if (!class_exists("resurs_submitLimitApplicationResponse", false)) 
{
class resurs_submitLimitApplicationResponse
{

    /**
     * @var limit $return
     * @access public
     */
    public $return = null;

    /**
     * @param limit $return
     * @access public
     */
    public function __construct($return)
    {
      $this->return = $return;
    }

}

}

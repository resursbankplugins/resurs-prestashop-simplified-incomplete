<?php

if (!class_exists("resurs_prepareSigning", false)) 
{
class resurs_prepareSigning
{

    /**
     * @var id $paymentSessionId
     * @access public
     */
    public $paymentSessionId = null;

    /**
     * @var anyURI $successUrl
     * @access public
     */
    public $successUrl = null;

    /**
     * @var anyURI $failUrl
     * @access public
     */
    public $failUrl = null;

    /**
     * @var boolean $forceSigning
     * @access public
     */
    public $forceSigning = null;

    /**
     * @param id $paymentSessionId
     * @param anyURI $successUrl
     * @param anyURI $failUrl
     * @access public
     */
    public function __construct($paymentSessionId, $successUrl, $failUrl)
    {
      $this->paymentSessionId = $paymentSessionId;
      $this->successUrl = $successUrl;
      $this->failUrl = $failUrl;
    }

}

}

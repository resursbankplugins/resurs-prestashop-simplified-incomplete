<?php

if (!class_exists("resurs_startPaymentSession", false)) 
{
class resurs_startPaymentSession
{

    /**
     * @var id $preferredId
     * @access public
     */
    public $preferredId = null;

    /**
     * @var id $paymentMethodId
     * @access public
     */
    public $paymentMethodId = null;

    /**
     * @var string $customerIpAddress
     * @access public
     */
    public $customerIpAddress = null;

    /**
     * @var nonEmptyString $formAction
     * @access public
     */
    public $formAction = null;

    /**
     * @var paymentSpec $paymentSpec
     * @access public
     */
    public $paymentSpec = null;

    /**
     * @param id $paymentMethodId
     * @param paymentSpec $paymentSpec
     * @access public
     */
    public function __construct($paymentMethodId, $paymentSpec)
    {
      $this->paymentMethodId = $paymentMethodId;
      $this->paymentSpec = $paymentSpec;
    }

}

}

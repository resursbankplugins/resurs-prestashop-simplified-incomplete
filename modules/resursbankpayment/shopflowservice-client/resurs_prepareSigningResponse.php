<?php

if (!class_exists("resurs_prepareSigningResponse", false)) 
{
class resurs_prepareSigningResponse
{

    /**
     * @var anyURI $return
     * @access public
     */
    public $return = null;

    /**
     * @param anyURI $return
     * @access public
     */
    public function __construct($return)
    {
      $this->return = $return;
    }

}

}

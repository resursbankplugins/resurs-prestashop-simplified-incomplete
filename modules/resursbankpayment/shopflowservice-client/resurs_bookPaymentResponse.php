<?php

if (!class_exists("resurs_bookPaymentResponse", false)) 
{
class resurs_bookPaymentResponse
{

    /**
     * @var bookingResult $return
     * @access public
     */
    public $return = null;

    /**
     * @param bookingResult $return
     * @access public
     */
    public function __construct($return)
    {
      $this->return = $return;
    }

}

}

<?php

if (!class_exists("resurs_submitLimitApplication", false)) 
{
class resurs_submitLimitApplication
{

    /**
     * @var id $paymentSessionId
     * @access public
     */
    public $paymentSessionId = null;

    /**
     * @var string $yourCustomerId
     * @access public
     */
    public $yourCustomerId = null;

    /**
     * @var nonEmptyString $formDataResponse
     * @access public
     */
    public $formDataResponse = null;

    /**
     * @var address $billingAddress
     * @access public
     */
    public $billingAddress = null;

    /**
     * @var int $bonusOverride
     * @access public
     */
    public $bonusOverride = null;

    /**
     * @param id $paymentSessionId
     * @param nonEmptyString $formDataResponse
     * @access public
     */
    public function __construct($paymentSessionId, $formDataResponse)
    {
      $this->paymentSessionId = $paymentSessionId;
      $this->formDataResponse = $formDataResponse;
    }

}

}

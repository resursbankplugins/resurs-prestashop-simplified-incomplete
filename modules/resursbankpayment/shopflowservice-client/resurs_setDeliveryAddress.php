<?php

if (!class_exists("resurs_setDeliveryAddress", false)) 
{
class resurs_setDeliveryAddress
{

    /**
     * @var id $paymentSessionId
     * @access public
     */
    public $paymentSessionId = null;

    /**
     * @var address $address
     * @access public
     */
    public $address = null;

    /**
     * @param id $paymentSessionId
     * @param address $address
     * @access public
     */
    public function __construct($paymentSessionId, $address)
    {
      $this->paymentSessionId = $paymentSessionId;
      $this->address = $address;
    }

}

}

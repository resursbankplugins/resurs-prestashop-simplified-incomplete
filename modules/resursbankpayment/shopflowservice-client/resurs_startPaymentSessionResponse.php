<?php

if (!class_exists("resurs_startPaymentSessionResponse", false)) 
{
class resurs_startPaymentSessionResponse
{

    /**
     * @var paymentSession $return
     * @access public
     */
    public $return = null;

    /**
     * @param paymentSession $return
     * @access public
     */
    public function __construct($return)
    {
      $this->return = $return;
    }

}

}

# Resurs Bank SimplifiedFlow for Prestashop

## WARNING

This plugin has not been maintained since summer 2015 and is not a complete solution for Prestashop. There are parts in the plugin that still needs to built and fixed.

Our intentions with this plugin is to be "inspiring" for future development of Prestashop Plugins.
The release of EComPHP, included in this package, is obsolete and is lacking compatiblity with both Resurs Checkout and Hosted flow.
